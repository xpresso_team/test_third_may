"""
This file contains generic utility functions
"""

import pandas as pd


def update_dictionary_recursively(master_dict: dict, current_dict: dict,
                                  current_depth: int, max_depth: int,
                                  key_prefix: str):
    """
    Update the dictionary with the keys and value depending on the max level
    Args:
        key_prefix:
        master_dict(dict): dictionary in which keys and value needs to be updated
        current_dict(dict): current dictionary
        current_depth(int):  current depth in the dictionary from th tree
        max_depth(int): Maximum depth allowed
    """

    for key, value in current_dict.items():
        if isinstance(value, dict) and current_depth < max_depth:
            update_dictionary_recursively(master_dict=master_dict,
                                          current_dict=value,
                                          current_depth=current_depth + 1,
                                          max_depth=max_depth,
                                          key_prefix=key+" > ")
        else:
            master_dict[key_prefix+key] = beautify(value)


def add_row(table_list, current_data, max_depth=2):
    """ Convert the dictionary item into a single level dictionary """
    if not isinstance(current_data, dict):
        return

    master_dictionary = dict()
    update_dictionary_recursively(master_dict=master_dictionary,
                                  current_dict=current_data,
                                  current_depth=0,
                                  max_depth=max_depth,
                                  key_prefix="")
    table_list.append(master_dictionary)


def generate_single_table_df(data, max_depth=2):
    """ Prints the data in tabular format """

    rows = []
    if isinstance(data, dict):
        add_row(rows, data, max_depth)
    elif isinstance(data, list):
        for item in data:
            if isinstance(item, dict):
                add_row(rows, item, max_depth)
            else:
                add_row(rows, {"default": item}, max_depth)
    else:
        add_row(rows, {"default": data}, max_depth)

    print_df = pd.DataFrame(rows)
    return print_df


def beautify_dict(dict_item: dict):
    """ Beautify dict items """
    response_list = []
    for key, value in dict_item.items():
        if isinstance(value, dict):
            response_list.append(f"{key}: [{beautify_dict(value)} ]")
        elif isinstance(value, list):
            response_list.append(f"{key}: [{beautify_list(value)} ]")
        else:
            response_list.append(f"{key}: [ {beautify(value)} ]")
    return ', '.join(response_list)


def beautify_list(list_item: list):
    """ Beautify list items """
    response_list = []
    for item in list_item:
        if isinstance(item, dict):
            response_list.append(beautify_dict(item))
        elif isinstance(item, list):
            response_list.append(beautify_list(item))
        else:
            response_list.append(beautify(item))
    return ' || '.join(response_list)


def beautify(item):
    """ Beautify any object """
    if isinstance(item, dict):
        return beautify_dict(item)
    elif isinstance(item, list):
        return beautify_list(item)
    else:
        return str(item)
