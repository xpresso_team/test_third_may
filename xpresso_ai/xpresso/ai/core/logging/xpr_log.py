""" Log management for Xpresso Project"""

__all__ = ["XprLogger"]
__author__ = "Srijan Sharma"

import json
import logging
import logging.handlers as log_handlers
import os

from logstash_async.constants import constants
from logstash_async.handler import AsynchronousLogstashHandler
from xpresso.ai.core.commons.utils.datetime_utils import DatetimeUtils

from xpresso.ai.core.commons.utils.xpr_request_context import XprRequestContext
from xpresso.ai.core.commons.exceptions.xpr_exceptions import InvalidConfigException
from xpresso.ai.core.commons.utils.constants import (
    LOG_TIMEFORMAT,
    HUNDRED_MB_IN_BYTES,
    COMPONENTNAME_KEY,
    PIPELINE_NAME_KEY,
    XPRESSO_RUN_NAME,
    PROJECT_NAME_KEY,
    XPR_WORKFLOW_KEY,
    PROJECT_NAME,
)
from xpresso.ai.core.logging.singleton_logger import SingletonLogger
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser

# This is intentional
constants.ERROR_LOG_RATE_LIMIT = "0 per minute"


class XprLogger(logging.Logger, metaclass=SingletonLogger):
    """
    Creates a logger object to put the logs into a file and index them into
    elastic search.

    Args:
        name(str): Name of the project name. This is used for categorizing logs
        level(int): Log level value
    """

    LOGGING_SECTION = "logging"
    LOG_HANDLER_SUBSECTION = "log_handler"
    LOGSTASH_HOST = "host"
    LOGSTASH_PORT = "port"
    LOG_LEVEL = "log_level"
    LOGSTASH_CACHE_BOOL = "cache_in_file"
    LOGGING_LOGSTASH_BOOL = "log_to_elk"
    LOGGING_FILE_BOOL = "log_to_file"
    LOGGING_CONSOLE_BOOL = "log_to_console"
    LOGS_FOLDER_PATH = "logs_folder_path"
    DEFAULT_LOGS_FOLDER_PATH = "default_folder_path"
    FORMATTER = "formatter"
    FIND_CONFIG_RECURSIVE = "find_config_recursive"

    FILE_BACKUP_COUNT = 5
    APPEND_MODE = "a"
    DEFAULT_PROJECT_NAME = "controller"

    def __init__(
        self,
        name=DEFAULT_PROJECT_NAME,
        config_path=None,
        level=None,
        component_name=None,
        pipeline_name=None,
        xpresso_run_name=None,
        xpresso_workflow=None,
    ):

        # Parameters to this method are passed to __call__ of singleton class and
        # ultimately used to set the context to it seems that they are unused here but they are used in __call__
        # so, don't remove any of above
        super().__init__(name)

    def set_logger_config(self):
        """
        Set/Update logger configuration using the logger_context
        """
        if self.xpresso_logs:
            self.logger_context = self.xpresso_logger_context
        else:
            self.logger_context = self.user_logger_context
        try:
            xpr_config_parse = XprConfigParser()
            logger_config_file_path = getattr(self.logger_context, "config_path")
            if logger_config_file_path:
                xpr_config_parse.update_logger_config(
                    logger_config_file_path=logger_config_file_path
                )
            self.xpr_config = xpr_config_parse[self.LOGGING_SECTION]
        except InvalidConfigException as e:
            # Use default configuration
            print(
                "Failed to read the config file: {0} Using default configuration for logger.".format(
                    e
                )
            )
            self.xpr_config = xpr_config_parse[self.LOGGING_SECTION]

        if self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.FIND_CONFIG_RECURSIVE]:
            self.xpr_config = self.load_config("xpr")

        level = getattr(self.logger_context, "level")
        if not getattr(self.logger_context, "level", None):
            # Take the default level from config.
            config_level = self.xpr_config[self.LOG_HANDLER_SUBSECTION].get(
                self.LOG_LEVEL, logging.INFO
            )
            # If no level is passed then use the config level otherwise use provided
            # one
            level = config_level
        self.setLevel(level)

        logger_formatter = XprCustomFormatter(
            self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.FORMATTER]
        )
        logstash_formatter = XprLogstashCustomFormatter(
            self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.FORMATTER]
        )
        log_folder = os.path.expanduser(
            self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.LOGS_FOLDER_PATH]
        )
        default_folder_path = os.path.expanduser(
            self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.DEFAULT_LOGS_FOLDER_PATH]
        )
        if not os.path.exists(log_folder):
            try:
                os.makedirs(log_folder, 0o777)
            except (PermissionError, IOError):
                pass
        else:
            try:
                for root, dirs, files in os.walk(log_folder):
                    for d in dirs:
                        os.chmod(os.path.join(root, d), 0o777)
                    for f in files:
                        os.chmod(os.path.join(root, f), 0o777)
            except (PermissionError, IOError):
                pass

        # Adding file handler for levels below warning
        if self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.LOGGING_FILE_BOOL]:
            if not self.set_file_handler(
                log_folder=log_folder,
                logger_formatter=logstash_formatter,
                level=level,
                extension="log",
            ):
                self.set_file_handler(
                    log_folder=default_folder_path,
                    logger_formatter=logstash_formatter,
                    level=level,
                    extension="log",
                )

        # Adding file handler for levels more critical than warning
        if self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.LOGGING_FILE_BOOL]:
            if not self.set_file_handler(
                log_folder=log_folder,
                logger_formatter=logstash_formatter,
                level=logging.ERROR,
                extension="err",
            ):
                self.set_file_handler(
                    log_folder=default_folder_path,
                    logger_formatter=logstash_formatter,
                    level=logging.ERROR,
                    extension="err",
                )

        # Adding logstash logging handler
        if self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.LOGGING_LOGSTASH_BOOL]:
            cache_filename = ""
            if self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.LOGSTASH_CACHE_BOOL]:
                cache_filename = os.path.join(log_folder, "cache.persistence")

            lh = AsynchronousLogstashHandler(
                host=self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.LOGSTASH_HOST],
                port=self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.LOGSTASH_PORT],
                database_path=cache_filename,
                event_ttl=120,
            )
            lh.setFormatter(logstash_formatter)
            al_handler_required = True
            for handler in self.handlers:
                if not isinstance(handler, log_handlers.RotatingFileHandler):
                    continue
                if handler.level == lh.level:
                    al_handler_required = False
            if al_handler_required:
                self.addHandler(lh)

        if self.xpr_config[self.LOG_HANDLER_SUBSECTION][self.LOGGING_CONSOLE_BOOL]:
            # Adding console logging handler
            stream_lh = logging.StreamHandler()
            stream_lh.setFormatter(logger_formatter)
            stream_handler_required = True
            for handler in self.handlers:
                if handler.__class__.__name__ == logging.StreamHandler.__name__:
                    stream_handler_required = False
            if stream_handler_required:
                self.addHandler(stream_lh)

    def set_file_handler(self, log_folder, logger_formatter, level, extension):
        """ Set file handler for the log """
        try:
            rfh = log_handlers.RotatingFileHandler(
                filename=os.path.join(
                    log_folder, ".".join((self.xpr_config[PROJECT_NAME], extension))
                ),
                mode=self.APPEND_MODE,
                maxBytes=HUNDRED_MB_IN_BYTES,
                backupCount=self.FILE_BACKUP_COUNT,
            )
            rfh.setFormatter(logger_formatter)
            rfh.setLevel(level)
            rf_handler_required = True
            for handler in self.handlers:
                if not isinstance(handler, log_handlers.RotatingFileHandler):
                    continue
                if (
                    handler.level == rfh.level
                    and handler.baseFilename == rfh.baseFilename
                ):
                    rf_handler_required = False
            if rf_handler_required:
                self.addHandler(rfh)
            return True
        except IOError:
            # failing_silently
            pass
        return False

    def filter(self, record):
        setattr(
            record,
            PROJECT_NAME,
            getattr(self.logger_context, PROJECT_NAME_KEY)
            if getattr(self.logger_context, PROJECT_NAME_KEY, None)
            else self.xpr_config[PROJECT_NAME],
        )
        setattr(
            record,
            COMPONENTNAME_KEY,
            getattr(self.logger_context, COMPONENTNAME_KEY, None),
        )
        setattr(
            record,
            PIPELINE_NAME_KEY,
            getattr(self.logger_context, PIPELINE_NAME_KEY, None),
        )
        setattr(
            record,
            XPRESSO_RUN_NAME,
            getattr(self.logger_context, XPRESSO_RUN_NAME, None),
        )
        setattr(
            record,
            XPR_WORKFLOW_KEY,
            getattr(self.logger_context, XPR_WORKFLOW_KEY, None),
        )
        return True

    def find_config(self, config_log_filename):
        """
        Iterates over the whole directory structure to find the path of the
        config file.

        Args:
            config_log_filename(str): name of the config file to be looked for

        Returns:
            str : Absolute Filepath of the configuration file

        """
        filepath = ""
        for dirpath, subdirs, files in os.walk(os.getcwd()):
            for x in files:
                if x.endswith(config_log_filename):
                    filepath = os.path.join(dirpath, x)
                    return filepath
        if not filepath:
            raise FileNotFoundError

    def load_config(self, config_log_type) -> XprConfigParser:
        """
        Args:
            config_log_type(str): Type of the config file i.e xpr or setup

        Returns:
            str : configuration filename based on the type provided

        """

        if config_log_type == "setup":
            config_log_filename = "setup_log.json"
        elif config_log_type == "xpr":
            config_log_filename = "xpr_log.json"
        else:
            raise ValueError("Invalid parameter passed to load_config")

        config = None
        try:
            config = XprConfigParser(
                os.path.join(self.find_config(config_log_filename))
            )
        except FileNotFoundError:
            # This is intended
            pass
        except InvalidConfigException as err:
            print(
                "Invalid config Found. Loading from the config from default"
                " path. \n{}".format(str(err))
            )
        finally:
            if config is None:
                try:
                    config = XprConfigParser()
                except FileNotFoundError as err:
                    print(
                        "Unable to file the config file in base directory. "
                        "Loading from the config from default path. \n"
                        "{}".format(str(err))
                    )
                    raise err
                except InvalidConfigException as err:
                    print("Invalid config Found. \n{}".format(str(err)))
                    raise err
        return config

    def __reduce__(self):
        return self.__class__, (self.name,)


class XprCustomFormatter(logging.Formatter):
    """
    Takes the record and formats the log
    in a very specific way
    """

    def __init__(self, formatter):
        super().__init__()
        self.formatter = formatter
        self.request_context = XprRequestContext()

    def format(self, record):
        log_values = dict()
        log_values["timestamp"] = DatetimeUtils.get_current_formatted_time(
            target_format=LOG_TIMEFORMAT
        )
        log_values[PROJECT_NAME] = getattr(record, PROJECT_NAME)
        log_values[COMPONENTNAME_KEY] = getattr(record, COMPONENTNAME_KEY, None)
        log_values[PIPELINE_NAME_KEY] = getattr(record, PIPELINE_NAME_KEY, None)
        log_values[XPRESSO_RUN_NAME] = getattr(record, XPRESSO_RUN_NAME, None)
        log_values[XPR_WORKFLOW_KEY] = getattr(record, XPR_WORKFLOW_KEY, None)
        for key in self.formatter:
            if self.formatter[key] and key == "request_id":
                key_map = self.request_context.get_request_id()
                log_values[key] = key_map
            elif self.formatter[key] and key != "request_id":
                log_values[str(key)] = str(getattr(record, key))
        return json.dumps(log_values)


class XprLogstashCustomFormatter(logging.Formatter):
    """
    Takes the record and formats the log
    in a very specific way
    """

    def __init__(self, formatter):
        super().__init__()
        self.formatter = formatter
        self.request_context = XprRequestContext()

    def format(self, record):
        log_values = dict()
        log_values["timestamp"] = DatetimeUtils.get_current_formatted_time(
            target_format=LOG_TIMEFORMAT
        )
        log_values[PROJECT_NAME] = getattr(record, PROJECT_NAME)
        log_values[COMPONENTNAME_KEY] = getattr(record, COMPONENTNAME_KEY, None)
        log_values[PIPELINE_NAME_KEY] = getattr(record, PIPELINE_NAME_KEY, None)
        log_values[XPRESSO_RUN_NAME] = getattr(record, XPRESSO_RUN_NAME, None)
        log_values[XPR_WORKFLOW_KEY] = getattr(record, XPR_WORKFLOW_KEY, None)
        for key in self.formatter:
            if self.formatter[key] and key == "request_id":
                key_map = self.request_context.get_request_id()
                log_values[key] = key_map
            elif self.formatter[key] and key != "request_id":
                log_values[str(key)] = str(getattr(record, key))
        return json.dumps(log_values)


if __name__ == "__main__":
    main_logger = XprLogger()
    main_logger.info("Test message")
