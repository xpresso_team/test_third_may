"""
    Module that defines model response schema
"""

__all__ = ["Response"]
__author__ = ["Shlok Chaudhari"]


from xpresso.ai.core.commons.utils.xpresso_base_enum import BaseEnum


class Response(BaseEnum):
    """
        Class that lists down all the labels
        for model response schema
    """
    MODEL_ID = "model_id"
    MODEL_REQUEST_ID = "model_request_id"
    TYPE = "type"
    TIME = "time"
    RESPONSE_DATA = "response_data"
    RESPONSE_TIME_MS = "response_time_ms"
    RESPONSE_HEADERS = "response_headers"
    RESPONSE_STATUS_CODE = "response_status_code"
    RESPONSE_SIZE_BYTES = "response_size_bytes"
