"""
    Model Monitoring Bridge to connect the request client and manager classes
"""

__all__ = ["ModelMonitoringBridge"]
__author__ = ["Shlok Chaudhari"]


from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.model_monitoring.manager import ModelMonitoringManager
from xpresso.ai.core.model_monitoring.request_client import HTTPRequestClient
from xpresso.ai.core.model_monitoring.metrics_publisher import MetricsPublisher
from xpresso.ai.core.commons.utils.constants import MODEL_MONITORING_SECTION, MAX_RETRIES


class ModelMonitoringBridge:
    """
        This is a bridge class used to connect all the
        supported functionalities by Model Monitoring
    """

    _logger = XprLogger()
    _config = XprConfigParser()

    def __init__(self, token=None):
        self._metrics_publisher = None
        self._mm_config = self._config[MODEL_MONITORING_SECTION]
        self._request_client = HTTPRequestClient(self._mm_config[MAX_RETRIES])
        self._mm_manager = ModelMonitoringManager(self._request_client, self._mm_config, token)

    def add_new_model(self, project_name: str, model_name: str, model_version: int):
        """
            Creates a new model entry in Model Monitoring
        Args:
            project_name(str): name of project
            model_name(str): name of deployed model
            model_version(int): version of deployed model
        Returns:
            created model details(dict)
        Raises:
            ModelMonitoringServiceFailed
        """
        self._logger.info("Adding new model for model monitoring")
        new_model_details = \
            self._mm_manager.create_new_model(project_name, model_name, model_version)
        self._logger.info("New model added for model monitoring")
        return new_model_details

    def report_operation_metrics(self, project_name: str, model_name: str, model_version: int):
        """
            Starts a background thread to report
            operation metrics for deployed model
        Args:
            project_name(str): name of project
            model_name(str): name of deployed model
            model_version(int): version of deployed model
        Raises:
            ModelMonitoringServiceFailed
        """
        self._logger.info("Reporting operation metrics to model monitoring")
        model_id = self._mm_manager.get_model_id(project_name, model_name, model_version)
        self._metrics_publisher = MetricsPublisher(model_id)

    def report_received_request(self, request_fields: dict):
        """
            Reports details of received requests to
            deployed model
        Args:
            request_fields(dict)
        """
        self._logger.info("Reporting details of received request to model monitoring")
        self._metrics_publisher.on_new_request(request_fields)

    def report_generated_response(self, response_fields: dict):
        """
            Reports details of generated response in
            deployed model
        Args:
            response_fields(dict)
        """
        self._logger.info("Reporting details of received request to model monitoring")
        self._metrics_publisher.on_response_ready(response_fields)
