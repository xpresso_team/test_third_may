from datetime import datetime
from jsonmerge import merge as json_merger

from xpresso.ai.core.commons.utils.datetime_utils import DatetimeUtils
from xpresso.ai.core.data.versioning.dv_field_name \
    import DataVersioningFieldName as DVFields



class PlotNames:
    LAST_MONTH_COMMITS = "last_month_datarepo_commit"
    LAST_MONTH_USER_COMMITS = "last_month_datarepo_commit_by_user"

    def __str__(self):
        return self.value


class VersioningPlots:
    def __init__(self):
        self.plot = None
        self.datetime_handler = DatetimeUtils()

    @staticmethod
    def generate_plot_dict(plot_name: str):
        """
        """
        plot_title_type_map = {
            PlotNames.LAST_MONTH_COMMITS: {
                "title": "No of commits per day in last month",
                "type": "line_chart"
            },
            PlotNames.LAST_MONTH_USER_COMMITS: {
                "title": "No of commits to data repo per user in last month",
                "type": "bar_chart"
            }
        }
        plot_format = {
            "title": str(),
            "category": "datarepo",
            "plot_type": "",
            "y_title": "count",
            "x_title": "data",
            "data": []
        }
        plot_info = json_merger(plot_format, plot_title_type_map[plot_name])
        return plot_info

    def update_last_month_commit_count(
         self, commit_on: datetime, last_month_commits: dict, commit_dates_map: dict):
        """

        Args:
            commit_on (datetime): [description]
            last_month_commits (dict): [description]
            commit_dates_map (dict): [description]
        """
        commit_on_str = self.datetime_handler.get_formatted_time(
            commit_on, "%a %d %B, %Y")
        if commit_on not in commit_dates_map:
            commit_dates_map[commit_on_str] = \
                len(last_month_commits["data"])
            last_month_commits["data"].append(
                {"count": 1, "date": commit_on_str}
            )
        else:
            date_index = commit_dates_map[commit_on_str]
            last_month_commits["data"][date_index]["count"] += 1

    @staticmethod
    def update_last_month_commit_user_count(
            commit_user: str, last_month_commit_users: dict,
            commit_users_map: dict):
        """

        Args:
            commit_user (str): [description]
            last_month_commit_users (dict): [description]
            commit_users_map (dict): [description]
        """
        user_info = last_month_commit_users["data"]
        if commit_user not in commit_users_map:
            commit_users_map[commit_user] = len(user_info)
            user_info.append(
                {commit_user: 1}
            )
        else:
            user_index = commit_users_map[commit_user]
            user_info[user_index][commit_user] += 1

    def get_branch_commits_plot(self, branch_info_list: list):
        """
        Args:
            branch_info_list: list of branches
        """
        last_month_commits_plot = \
            self.generate_plot_dict(PlotNames.LAST_MONTH_COMMITS)
        last_month_commit_users_plot = \
            self.generate_plot_dict(PlotNames.LAST_MONTH_USER_COMMITS)
        commit_dates_map = dict()
        commit_users_map = dict()
        for branch_item in branch_info_list:
            for commit_item in branch_item[DVFields.BRANCH_COMMITS_KEY.value]:
                commit_user = commit_item[DVFields.COMMITTED_BY.value]
                commit_on = \
                    DatetimeUtils.convert_str_time_to_datetime(
                        commit_item[DVFields.COMMITTED_ON.value])
                delay_flag = \
                    DatetimeUtils.check_if_date_is_in_30_days(commit_on)
                if not delay_flag:
                    continue
                self.update_last_month_commit_count(
                    commit_on, last_month_commits_plot, commit_dates_map)
                self.update_last_month_commit_user_count(
                    commit_user, last_month_commit_users_plot, commit_users_map)
        plot_info = {
            PlotNames.LAST_MONTH_COMMITS: last_month_commits_plot,
            PlotNames.LAST_MONTH_USER_COMMITS: last_month_commit_users_plot
        }
        return plot_info
