__all__ = ["ExploreDate"]
__author__ = ["Srijan Sharma"]

import pandas as pd
from xpresso.ai.core.commons.utils.constants import DAY
from datetime import timedelta


class ExploreDate():

    def __init__(self, data):
        self.data = data
        return

    def populate_date(self):
        """Sets the metrics for date type attribute"""
        resp = dict()
        min_date, max_date, day_count, month_count, year_count, missing_dates\
            = \
            self.date_analysis(self.data)
        resp["min"] = min_date
        resp["max"] = max_date
        resp["day_count"] = day_count
        resp["month_count"] = month_count
        resp["year_count"] = year_count
        resp["missing_dates"] = missing_dates
        return resp

    @staticmethod
    def date_analysis(data):
        """Performs analysis on date attribute values
        Args:
            data (:obj:`list`): records of date attribute
        Returns:
            min (:obj:`Timestamp`): Minimum value of date
            max (:obj:`Timestamp`): Maximum value of date
            day_count, (`int`): Count of number of days
            month_count, (`int`): Count of number of month
            year_count, (`int`): Count of number of years
            missing_date_tuple (:obj:`list`): list of tuples of missing date
            ranges
            """
        data = pd.to_datetime(data, infer_datetime_format=True)
        min = data.min().strftime("%d-%m-%Y")
        max = data.max().strftime("%d-%m-%Y")
        date_df = pd.DataFrame({"day": data.dt.day_name(),
                                "month": data.dt.month_name(),
                                "year": data.dt.year.values})

        day_count = date_df["day"].dropna().value_counts().to_dict()
        month_count = date_df["month"].dropna().value_counts().to_dict()
        year_count = date_df["year"].dropna().astype("int").value_counts(
        ).to_dict()

        dates = pd.to_datetime(data, infer_datetime_format=True)
        dates = dates[dates.notnull()].to_list()
        dates = [d.date() for d in dates]

        unique_dates = sorted(set(dates))
        all_dates = pd.date_range(start=min, end=max, freq=DAY)
        missing_dates = sorted(set(all_dates) - set(unique_dates))
        missing_date_tuple = [((previous + timedelta(1)).strftime("%d-%m-%Y"),
                               (current - timedelta(1)).strftime("%d-%m-%Y"))
                              for previous, current in
                              zip(unique_dates[:-1], unique_dates[1:])
                              if current != previous + timedelta(1)]
        return min, max, day_count, month_count, year_count, missing_date_tuple
